personas package
================

Submodules
----------

personas.admin module
---------------------

.. automodule:: personas.admin
    :members:
    :undoc-members:
    :show-inheritance:

personas.forms module
---------------------

.. automodule:: personas.forms
    :members:
    :undoc-members:
    :show-inheritance:

personas.models module
----------------------

.. automodule:: personas.models
    :members:
    :undoc-members:
    :show-inheritance:

personas.tests module
---------------------

.. automodule:: personas.tests
    :members:
    :undoc-members:
    :show-inheritance:

personas.views module
---------------------

.. automodule:: personas.views
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: personas
    :members:
    :undoc-members:
    :show-inheritance:
