lib package
===========

Submodules
----------

lib.funciones module
--------------------

.. automodule:: lib.funciones
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: lib
    :members:
    :undoc-members:
    :show-inheritance:
