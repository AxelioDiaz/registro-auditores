curriculum package
==================

Subpackages
-----------

.. toctree::

    curriculum.templatetags

Submodules
----------

curriculum.admin module
-----------------------

.. automodule:: curriculum.admin
    :members:
    :undoc-members:
    :show-inheritance:

curriculum.forms module
-----------------------

.. automodule:: curriculum.forms
    :members:
    :undoc-members:
    :show-inheritance:

curriculum.models module
------------------------

.. automodule:: curriculum.models
    :members:
    :undoc-members:
    :show-inheritance:

curriculum.tests module
-----------------------

.. automodule:: curriculum.tests
    :members:
    :undoc-members:
    :show-inheritance:

curriculum.urls module
----------------------

.. automodule:: curriculum.urls
    :members:
    :undoc-members:
    :show-inheritance:

curriculum.views module
-----------------------

.. automodule:: curriculum.views
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: curriculum
    :members:
    :undoc-members:
    :show-inheritance:
