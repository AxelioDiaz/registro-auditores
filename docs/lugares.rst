lugares package
===============

Submodules
----------

lugares.admin module
--------------------

.. automodule:: lugares.admin
    :members:
    :undoc-members:
    :show-inheritance:

lugares.models module
---------------------

.. automodule:: lugares.models
    :members:
    :undoc-members:
    :show-inheritance:

lugares.tests module
--------------------

.. automodule:: lugares.tests
    :members:
    :undoc-members:
    :show-inheritance:

lugares.views module
--------------------

.. automodule:: lugares.views
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: lugares
    :members:
    :undoc-members:
    :show-inheritance:
