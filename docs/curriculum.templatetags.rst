curriculum.templatetags package
===============================

Submodules
----------

curriculum.templatetags.perfil module
-------------------------------------

.. automodule:: curriculum.templatetags.perfil
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: curriculum.templatetags
    :members:
    :undoc-members:
    :show-inheritance:
