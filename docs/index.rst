.. Auditores documentation master file, created by
   sphinx-quickstart on Tue Apr 22 08:52:51 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Auditores's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 4

   auditores_suscerte
   auth
   curriculum
   lib
   lugares
   manage
   passwords
   personas


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

